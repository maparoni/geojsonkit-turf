import XCTest
@testable import GeoJSONKitTurf

XCTMain([
  testCase(GeoJSONKitTurfTests.allTests)
])
